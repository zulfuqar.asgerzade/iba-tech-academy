let startPage = document.querySelector('.start-page');
startPage.style.height = window.innerHeight + 'px';

let centerTextContainer = document.querySelector('.center-text-container');
let navbarHeight = document.querySelector('.header-container').clientHeight;
centerTextContainer.style.margin = (window.innerHeight - navbarHeight - (centerTextContainer.offsetHeight + 8)) / 2 + 'px 0px';


function clearClass(className, clearClass) {
    let serviceButtons = document.getElementsByClassName(className);
    
    for (let i = 0; i < serviceButtons.length; i++) {
        
        if(serviceButtons[i].className.includes(clearClass)) {
            serviceButtons[i].classList.remove(clearClass);
            break;
        }   
    }
}



// Services

let services = [
    {
        service: 'Web Design',
        serviceImg: './img/services/WebDesign.jpg',
        serviceDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias repellendus placeat cum ut numquam iste rem cupiditate, repellat praesentium possimus doloribus recusandae harum impedit quod vero quo quos, ratione quam nam, odit iure! Vitae quisquam quod nemo nesciunt eligendi ullam aut sit repudiandae ducimus esse nam, molestiae a velit alias corrupti, ad ratione officiis praesentium. Placeat dignissimos repellat alias corrupti porro quas laboriosam nisi, dolores aliquid harum quos debitis suscipit officia voluptate eum! Optio tempora repellendus eos accusantium est? Nam assumenda eos, eligendi eveniet laudantium architecto beatae, pariatur omnis unde nobis quisquam ducimus temporibus modi minima? Cumque sunt ad asperiores aliquam molestias fugit ratione vel. Perspiciatis et voluptatibus maxime ad. Veritatis vitae non quod dolores! Enim tempora, laudantium id quod voluptas rerum necessitatibus a, quia, laboriosam voluptates beatae. Eligendi, veritatis ipsa officiis, totam, tenetur quisquam nulla quod consequatur dolorem inventore tempora vel dolorum aperiam vero soluta cum. Ducimus iusto amet neque veritatis blanditiis, quo soluta dignissimos quasi! A eligendi incidunt nulla, accusantium doloribus totam quod autem! Iusto necessitatibus libero impedit voluptas, sed adipisci excepturi maxime praesentium optio.'
    },
    {
        service: 'Graphic Design',
        serviceImg: './img/services/graphicDesign.jpg',
        serviceDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias repellendus placeat cum ut numquam iste rem cupiditate, repellat praesentium possimus doloribus recusandae harum impedit quod vero quo quos, ratione quam nam, odit iure! Vitae quisquam quod nemo nesciunt eligendi ullam aut sit repudiandae ducimus esse nam, molestiae a velit alias corrupti, ad ratione officiis praesentium. Placeat dignissimos repellat alias corrupti porro quas laboriosam nisi, dolores aliquid harum quos debitis suscipit officia voluptate eum! Optio tempora repellendus eos accusantium est? Nam assumenda eos, eligendi eveniet laudantium architecto beatae, pariatur omnis unde nobis quisquam ducimus temporibus modi minima? Cumque sunt ad asperiores aliquam molestias fugit ratione vel. Perspiciatis et voluptatibus maxime ad. Veritatis vitae non quod dolores! Enim tempora, laudantium id quod voluptas rerum necessitatibus a, quia, laboriosam voluptates beatae. Eligendi, veritatis ipsa officiis, totam, tenetur quisquam nulla quod consequatur dolorem inventore tempora vel dolorum aperiam vero soluta cum. Ducimus iusto amet neque veritatis blanditiis, quo soluta dignissimos quasi!'
    },
    {
        service: 'Online Support',
        serviceImg: './img/services/OnlineSupport.jpg',
        serviceDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias repellendus placeat cum ut numquam iste rem cupiditate, repellat praesentium possimus doloribus recusandae harum impedit quod vero quo quos, ratione quam nam, odit iure! Vitae quisquam quod nemo nesciunt eligendi ullam aut sit repudiandae ducimus esse nam, molestiae a velit alias corrupti, ad ratione officiis praesentium. Placeat dignissimos repellat alias corrupti porro quas laboriosam nisi, dolores aliquid harum quos debitis suscipit officia voluptate eum! Optio tempora repellendus eos accusantium est? Nam assumenda eos, eligendi eveniet laudantium architecto beatae, pariatur omnis unde nobis quisquam ducimus temporibus modi minima? Cumque sunt ad asperiores aliquam molestias fugit ratione vel. Perspiciatis et voluptatibus maxime ad. Veritatis vitae non quod dolores! Enim tempora, laudantium id quod voluptas rerum necessitatibus a, quia, laboriosam voluptates beatae. Eligendi, veritatis ipsa officiis, totam, tenetur quisquam nulla quod consequatur dolorem inventore tempora vel dolorum aperiam vero soluta cum. Ducimus iusto amet neque veritatis blanditiis, quo soluta dignissimos quasi! A eligendi incidunt nulla, accusantium doloribus totam quod autem! Iusto necessitatibus libero impedit voluptas, sed adipisci excepturi maxime praesentium optio.'
    },
    {
        service: 'App Design',
        serviceImg: './img/services/AppDesign.jpg',
        serviceDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias repellendus placeat cum ut numquam iste rem cupiditate, repellat praesentium possimus doloribus recusandae harum impedit quod vero quo quos, ratione quam nam, odit iure! Vitae quisquam quod nemo nesciunt eligendi ullam aut sit repudiandae ducimus esse nam, molestiae a velit alias corrupti, ad ratione officiis praesentium. Placeat dignissimos repellat alias corrupti porro quas laboriosam nisi, dolores aliquid harum quos debitis suscipit officia voluptate eum! Optio tempora repellendus eos accusantium est? Nam assumenda eos, eligendi eveniet laudantium architecto beatae, pariatur omnis unde nobis quisquam ducimus temporibus modi minima? Cumque sunt ad asperiores aliquam molestias fugit ratione vel. Perspiciatis et voluptatibus maxime ad. Veritatis vitae non quod dolores! Enim tempora, laudantium id quod voluptas rerum necessitatibus a, quia, laboriosam voluptates beatae. Eligendi, veritatis ipsa officiis, totam, tenetur quisquam nulla quod consequatur dolorem inventore tempora vel dolorum aperiam vero soluta cum. Ducimus iusto amet neque veritatis blanditiis, quo soluta dignissimos quasi!'
    },
    {
        service: 'Online Marketing',
        serviceImg: './img/services/OnlineMarketing.jpg',
        serviceDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias repellendus placeat cum ut numquam iste rem cupiditate, repellat praesentium possimus doloribus recusandae harum impedit quod vero quo quos, ratione quam nam, odit iure! Vitae quisquam quod nemo nesciunt eligendi ullam aut sit repudiandae ducimus esse nam, molestiae a velit alias corrupti, ad ratione officiis praesentium. Placeat dignissimos repellat alias corrupti porro quas laboriosam nisi, dolores aliquid harum quos debitis suscipit officia voluptate eum! Optio tempora repellendus eos accusantium est? Nam assumenda eos, eligendi eveniet laudantium architecto beatae, pariatur omnis unde nobis quisquam ducimus temporibus modi minima? Cumque sunt ad asperiores aliquam molestias fugit ratione vel. Perspiciatis et voluptatibus maxime ad. Veritatis vitae non quod dolores! Enim tempora, laudantium id quod voluptas rerum necessitatibus a, quia, laboriosam voluptates beatae. Eligendi, veritatis ipsa officiis, totam, tenetur quisquam nulla quod consequatur dolorem inventore tempora vel dolorum aperiam vero soluta cum. Ducimus iusto amet neque veritatis blanditiis, quo soluta dignissimos quasi! A eligendi incidunt nulla, accusantium doloribus totam quod autem! Iusto necessitatibus libero impedit voluptas, sed adipisci excepturi maxime praesentium optio.'
    },
    {
        service: 'Seo Service',
        serviceImg: './img/services/SeoService.jpg',
        serviceDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias repellendus placeat cum ut numquam iste rem cupiditate, repellat praesentium possimus doloribus recusandae harum impedit quod vero quo quos, ratione quam nam, odit iure! Vitae quisquam quod nemo nesciunt eligendi ullam aut sit repudiandae ducimus esse nam, molestiae a velit alias corrupti, ad ratione officiis praesentium. Placeat dignissimos repellat alias corrupti porro quas laboriosam nisi, dolores aliquid harum quos debitis suscipit officia voluptate eum! Optio tempora repellendus eos accusantium est? Nam assumenda eos, eligendi eveniet laudantium architecto beatae, pariatur omnis unde nobis quisquam ducimus temporibus modi minima? Cumque sunt ad asperiores aliquam molestias fugit ratione vel. Perspiciatis et voluptatibus maxime ad. Veritatis vitae non quod dolores! Enim tempora, laudantium id quod voluptas rerum necessitatibus a, quia, laboriosam voluptates beatae. Eligendi, veritatis ipsa officiis, totam, tenetur quisquam nulla quod consequatur dolorem inventore tempora vel dolorum aperiam vero soluta cum. Ducimus iusto amet neque veritatis blanditiis, quo soluta dignissimos quasi!'
    },
];


function changeServiceInfo(serviceType) {
    for (const element in services) {
        if (services[element].service == serviceType) {
            let serviceImage = document.querySelector('.service-img');
            serviceImage.src = services[element].serviceImg;
            let serviceDescription = document.querySelector('.service-title');
            serviceDescription.textContent = services[element].serviceDescription;
        }
    }
}

let servicesContainer = document.querySelector('.service-list-container');
servicesContainer.addEventListener('click', (event) => {
    changeServiceInfo(event.target.textContent);
    clearClass('service-btn', 'service-btn-active');
    event.target.classList.add('service-btn-active');    
});




// Our Works

function showImages(workType, iterationCount, fileExtension = 'jpg') {

    workLoadBtn.remove();
    let workImgContainer = document.querySelector('.work-images-container');
    console.log(workImgContainer);
    workImgContainer.innerHTML = '';


    for (let i = 1; i <= iterationCount; i++) {
             
        let workImg = document.createElement('li');
        workImg.classList.add('work-image');
        
        let img = document.createElement('img');
        img.src = './img/works/' + workType + '/' + workType + i + '.' + fileExtension;
        workImg.appendChild(img);

        let workImgHover = document.createElement('div');
        workImgHover.classList.add('work-image-hover');

        let imgBtnContainer = document.createElement('div');
        imgBtnContainer.classList.add('work-img-btn-container');

        let imgLinkBtn = document.createElement('button');
        imgLinkBtn.classList.add('work-img-btn');
        imgLinkBtn.classList.add('work-link-btn');

        let imgSearchBtn = document.createElement('button');
        imgSearchBtn.classList.add('work-img-btn');
        imgSearchBtn.classList.add('work-search-btn');

        let imgHoverTxt = document.createElement('a');
        imgHoverTxt.classList.add('work-img-hover-txt');
        imgHoverTxt.textContent = 'creative design';

        let workTypeTxt = document.createElement('p');
        workTypeTxt.classList.add('work-img-type');
        workTypeTxt.textContent = workType;

        imgBtnContainer.appendChild(imgLinkBtn);
        imgBtnContainer.appendChild(imgSearchBtn);

        workImgHover.appendChild(imgBtnContainer);
        workImgHover.appendChild(imgHoverTxt);
        workImgHover.appendChild(workTypeTxt);

        workImg.appendChild(workImgHover);


        workImgContainer.appendChild(workImg);
    }
}



let workCategory = document.querySelector('.our-work-navbar');
workCategory.addEventListener('click', () => {
    console.log(event.target.textContent);
    
    if (event.target.textContent == 'All') {
        showImages('all', 12, 'png');
    }
    else if(event.target.textContent == 'Graphic Design') {
        showImages('graphic design', 12);
    }
    else if(event.target.textContent == 'Web Design') {
        showImages('web design', 7);
    }
    else if(event.target.textContent == 'Landing Pages') {
        showImages('landing page', 7);
    }
    else {
        showImages('wordpress', 10);
    }

    clearClass('our-work-navbar-btn', 'our-work-navbar-btn-active');
    event.target.classList.add('our-work-navbar-btn-active');
});


let workLoadBtn = document.querySelector('.work-img-load-btn');
workLoadBtn.addEventListener('click', () => {
    let workImagesContainer = document.querySelector('.work-images-container');
    workImagesContainer.style.height = '680px';
    setTimeout(() => {
        workLoadBtn.remove();
    }, 300);
});