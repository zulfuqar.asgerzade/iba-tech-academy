function createNewUser(name, surname) {

    let newUser = {};
    let userDate = prompt("Enter date", "dd-mm-yyyy");

    Object.defineProperties(newUser, {
        firstName: {
            get() { return this.firstname },
            set(name) { this.firstname = name }
        },
        lastName: {
            get() { return this.lastname },
            set(name) { this.lastname = name }
        }
    });

    newUser.firstname = name;
    newUser.lastname = surname;
    newUser.getLogin = function () {
        return (this.firstname.charAt(0) + this.lastname).toLowerCase();
    }
    newUser.getAge = function () {
        let date = new Date(userDate);
        var ageDifMs = Date.now() - date.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    newUser.getPassword = function () {
        return  this.getLogin() + new Date(userDate).getFullYear();
    }

    return newUser;
}

let newUser = createNewUser(prompt("Enter name:"), prompt("Enter surname:"));
console.log(newUser);
console.log("User age: " + newUser.getAge());
console.log("User login: " + newUser.getLogin());
console.log("User password: " + newUser.getPassword());