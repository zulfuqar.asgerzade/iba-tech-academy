function checkData(data) {
    let regexNum = new RegExp('^[0-9]+$');
    let regexOperation = new RegExp('^[+|()-|*|/]+$');

    if (data.match(regexNum)) {
        return "number";
    }
    else if (data.match(regexOperation)) {
        return "operation";
    }
    else {
        return false;
    }
}

let frstNum = prompt("Enter first num:");

while (!(checkData(frstNum) == "number")) {
    frstNum = prompt("Enter first num:");
}

let matchOperation = prompt("Enter operation");

while (!(checkData(matchOperation) == "operation")) {
    matchOperation = prompt("Enter operation");
}

let scndNum = prompt("Enter second num:");

while (!(checkData(scndNum) == "number")) {
    scndNum = prompt("Enter second num:");
}


function calculate(num1, num2, operation) {
    return eval(parseInt(num1)  + operation + parseInt(num2));
}

console.log(calculate(frstNum, scndNum, matchOperation));

