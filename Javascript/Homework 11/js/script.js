let btns = document.getElementsByClassName('btn');

function checkColor() {
    for (let i = 0; i < btns.length; i++) {
        if(btns[i].style.backgroundColor == 'blue') {
            btns[i].style.backgroundColor = '#33333a';
        }        
    }
}

function checkBtn(content) {
    for (let i = 0; i < btns.length; i++) {
        if(btns[i].textContent == content) {
            btns[i].style.backgroundColor = 'blue';
            break;
        }
    }
}

document.addEventListener('keydown', () => {
    checkColor();
    checkBtn(event.key);
    console.log(event.key);
});