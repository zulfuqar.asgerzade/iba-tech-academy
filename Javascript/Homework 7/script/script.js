let arr = ['hello', 'world', ['1', '2', '3', 'sea', 'user', 23], 'Baku', 'IBA Tech Academy', '2019'];

function showArr(collection) {

    let mainUl = document.createElement('ul');

    collection.map(item => {

        if (typeof (item) == "object") {

            let subUl = document.createElement('ul');

            item.map(element => {
                subUl.innerHTML += `<li>${element}</li>`;
                mainUl.appendChild(subUl);
            });
        }
        else {
            mainUl.innerHTML += `<li>${item}</li>`;
        }
    });

    document.body.querySelector('script').before(mainUl);
}

showArr(arr);


function startTimer(duration, display) {

    let timer = duration, seconds;
    
    setInterval(function () {
        
        seconds = parseInt(timer % 60, 10);
        seconds = seconds < 10 ? "0" + seconds : seconds;
        display.textContent = "0:" + seconds;

        if (--timer < 0) {
            timer = duration;

            if (timer == duration) {
                document.body.innerHTML = '';
            }
        }

    }, 1000);
}

window.onload = function () {
    let tenSeconds = 10;
    let display = document.querySelector('#time');
    startTimer(tenSeconds, display);
};
