function createNewUser(name, surname) {

    let newUser = {};

    Object.defineProperties(newUser, {
        firstName: {
            get() { return this.firstname },
            set(name) { this.firstname = name }
        },
        lastName: {
            get() { return this.lastname },
            set(name) { this.lastname = name }
        },
    });

    newUser.firstname = name;
    newUser.lastname = surname;
    newUser.getLogin = function () {
        return (this.firstname.charAt(0) + this.lastName).toLowerCase();
    }

    return newUser;
}

let newUser = createNewUser(prompt("Enter name:"), prompt("Enter surname:"));
console.log(newUser);
console.log("User login: " + newUser.getLogin());