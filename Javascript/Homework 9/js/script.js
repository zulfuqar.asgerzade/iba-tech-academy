let centerNavbar = document.querySelector('.tabs');
let btns = document.querySelectorAll('.tabs > li')

btns.forEach((btn, index, array) => {
    btn.dataset.id = index
});

function clearClass() {
    let elements = document.getElementsByClassName('tabs-title');

    for(let i = 0; i < elements.length; i++) {
        if(elements[i].classList.contains('active')) {
            elements[i].classList.remove('active');
        }
    }
}

let texts = document.getElementsByClassName('hiddenTxt');

centerNavbar.addEventListener('click', () => {
    clearClass();
    event.target.classList.add('active');
    let x = document.querySelector(`li[dataType=${event.target.textContent}`);
    x.classList.remove('hiddenTxt');
});