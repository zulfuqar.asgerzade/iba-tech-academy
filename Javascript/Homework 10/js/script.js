let frstPasswordIcon = document.getElementsByClassName('icon-password')[0];
let scndPasswordIcon = document.getElementsByClassName('icon-password')[1];

function checkInputType(input) {
    if(input.type == 'text') {
        input.type = 'password';
    }
    else if (input.type == 'password') {
        input.type = 'text'
    }
}

let firstPass = document.querySelector('.frstPass');
frstPasswordIcon.addEventListener('click', () => {
    checkInputType(firstPass);
    frstPasswordIcon.classList.toggle('fa-eye-slash');
});

let scndPass = document.querySelector('.scndPass');
scndPasswordIcon.addEventListener('click', () => {
    checkInputType(scndPass);
    scndPasswordIcon.classList.toggle('fa-eye-slash');
});


let errorMsg = document.createElement('span');
let confirmBtn = document.querySelector('.btn');
confirmBtn.addEventListener('click', () => {
    if (firstPass.value == scndPass.value) {
        alert('You are welcome!');
        firstPass.value = '';
        scndPass.value = '';
        frstPasswordIcon.classList.remove('fa-eye-slash');
        scndPasswordIcon.classList.remove('fa-eye-slash');
        checkInputType(firstPass);
        checkInputType(scndPass);
        errorMsg.textContent = '';
        
    }
    else {
        errorMsg.textContent = 'You need to enter the identical values!';
        errorMsg.style.color = 'red';
        document.getElementsByClassName('input-wrapper')[1].after(errorMsg);
    }
});


