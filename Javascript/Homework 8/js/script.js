let inputPrice = document.querySelector('.input-price');

inputPrice.addEventListener('focus', () => {
    inputPrice.style.display = 'block';
    inputPrice.style.outlineColor = 'green';
    inputPrice.style.color = 'black';
});

let price = document.createElement('span');
let errorMsg = document.createElement('span');
let cancelBtn = document.createElement('button');
cancelBtn.textContent = 'X';
cancelBtn.style.marginLeft = '5px';

cancelBtn.onclick = () => {
    price.remove();
    cancelBtn.remove();
    inputPrice.value = '';
}

inputPrice.addEventListener('focusout', () => {
    
    if (inputPrice.value > 0) {
        inputPrice.style.color = 'green';
        price.textContent = `Current price: ${inputPrice.value}`;
        cancelBtn.style.visibility = 'visible';
        errorMsg.textContent = '';

        document.querySelector('.input-price').before(price);
        document.querySelector('.input-price').before(cancelBtn);
    }
    else {
        inputPrice.style.color = 'red';
        inputPrice.style.outlineColor = 'red';
        errorMsg.textContent = 'Please insert correct number!';
        errorMsg.style.color = 'red';

        inputPrice.after(errorMsg);
    }
});