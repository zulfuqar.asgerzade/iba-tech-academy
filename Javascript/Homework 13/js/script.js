function changeColor(color) {
    let navbarBtns = document.querySelectorAll('.navbar ul li a');
    navbarBtns.forEach(element => {
        element.style.color = color;
    });
}

changeColor(localStorage.getItem('navbarColor'));

let counter = 0;
let changeThemeBtn = document.querySelector('.change-theme-btn');
changeThemeBtn.addEventListener('click', () => {

    if(counter == 0) {
        localStorage.setItem('navbarColor', 'red');
        counter++;
    }
    else {
        localStorage.setItem('navbarColor', '#16a085');
        counter--;
    }

    changeColor(localStorage.getItem('navbarColor'));
});